#
# Cookbook Name:: server
# Recipe:: packages
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


# git
# php54
# php54-mbstring
# php54-mysql
# php54-gd
# php54-xml
# php54-devel
# php54-pear
# php54-pecl-apc
# nginx
# httpd
# mod_ssl
# mysql-server
%w/
git
nginx
/.each do |app|
  package app do
    action :install
  end
end


# end
