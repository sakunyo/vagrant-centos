#
# Cookbook Name:: server
# Recipe:: epel-release
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#



# epel-release - Extra Packages for Enterprise Linux repository configuration
#
# http://ftp.riken.jp/Linux/fedora/epel/6/x86_64/repoview/epel-release.html
#
script "feature_repos" do
  interpreter "bash"
  cwd "/vagrant"
  user "root"
  code <<-EOC

  wget http://ftp.riken.jp/Linux/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm
  wget http://dl.iuscommunity.org/pub/ius/stable/Redhat/6/x86_64/ius-release-1.0-11.ius.el6.noarch.rpm
  wget http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
  wget http://nginx.org/packages/centos/6/noarch/RPMS/nginx-release-centos-6-0.el6.ngx.noarch.rpm

  rpm -ivh epel-release-6-8.noarch.rpm
  rpm -ivh ius-release-1.0-11.ius.el6.noarch.rpm
  rpm -ivh remi-release-6.rpm
  rpm -ivh nginx-release-centos-6-0.el6.ngx.noarch.rpm

  EOC
  not_if "test -e /vagrant/epel-release-6-8.noarch.rpm"
end


# end
