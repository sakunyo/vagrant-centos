#
# Cookbook Name:: server
# Recipe:: default
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


# Setup and Package Install
#
include_recipe "server::epel-release"
include_recipe "server::packages"
include_recipe "server::ssl"


# Application Configuration
#
include_recipe "server::iptables.setup"
include_recipe "server::nginx.setup"
include_recipe "server::rbenv"

# include_recipe "webapp::conf"
# include_recipe "webapp::httpd"
# include_recipe "webapp::mysqld"
# include_recipe "webapp::nodejs"
# include_recipe "webapp::service"


# StartUp Services
#
include_recipe "server::service.run"


# end
