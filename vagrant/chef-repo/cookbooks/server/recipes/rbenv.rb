#
# Cookbook Name:: server
# Recipe:: rbenv
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


git '/home/vagrant/.rbenv' do
	repository 'git://github.com/sstephenson/rbenv.git'
	reference "master"
	action :sync
end


# bash 'add_to__bash_profile' do
# 	user 'vagrant'
# 	cwd '/home/vagrant'
# 	code <<-EOH
# 	echo 'hoge=9' >> /home/vagrant/.bash_profile
# 	EOH
# end


# end
