#
# Cookbook Name:: server
# Recipe:: service.run
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


service 'nginx' do
  action :start
end


bash "iptables Restart" do
  user "root"
  code <<-EOC
  /etc/init.d/iptables restart
  EOC
end


# end
