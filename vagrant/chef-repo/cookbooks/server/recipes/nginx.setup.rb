#
# Cookbook Name:: server
# Recipe:: nginx.setup
#
# Copyright 2013, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#


# nginx.conf
cookbook_file '/etc/nginx/nginx.conf' do
  owner 'root'
  group 'root'
  source 'nginx/nginx.conf'
end


# conf.d
cookbook_file '/etc/nginx/conf.d/default.conf' do
  owner 'root'
  group 'root'
  source 'nginx/conf.d/default.conf'
end


# end
