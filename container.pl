#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

if ($#ARGV gt -1) {
	if ( -e "vagrant-data") {
		system "rm", "vagrant-data";
	}

	system "ln", "-s", "$ARGV[0]", "vagrant-data";
}
